#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	List<char> mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('a');
	mylist.pushToHead('n');
	mylist.pushToHead('b');
	mylist.pushToHead('h');
	mylist.pushToHead('g');
	mylist.pushToTail('e');
	mylist.pushToTail('d');
	cout << "Print all element in list : "; mylist.print(); cout << endl;
    cout << "Pophead : " << mylist.popHead() << endl;
	cout << "Poptail : " << mylist.popTail() << endl;
	cout << "Print after pop head-tail : "; mylist.print();
	
	cout << endl << "Search data isn't in list :"<< mylist.search('j');
	cout << endl << "Search data is in list :"<< mylist.search('k');

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?

}