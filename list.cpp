#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	tail = new Node(el, 0, tail);
	if (head == 0) {
		head = tail;
	}
	else {
		tail->prev->next = tail;
	}
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	char el = tail->data;
	Node* tmp = tail;
	if (head == tail) {
		head = tail = 0;
	}
	else {
		tail = tail->prev;
		tail->next = 0;
	}
	delete tmp;
	return el;
}
bool List::search(char el)
{
	bool check = false;
	Node* tmp1 = head;
	Node* tmp2 = tail;
	while (check == false) {
		if (tmp1->data == el || tmp2->data == el) { //search 2 way from head->tail and tail->head.
			check = true;
		}
		if (tmp1 == tmp2 || tmp1->next==tmp2 || tmp2->prev==tmp1) { //do not use to much processing.
			break;
		}
		tmp1 = tmp1->next;
		tmp2 = tmp2->prev;	
	}
	return check;
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}