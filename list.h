#ifndef LIST
#define LIST
#include <iostream>
#include <vector>
using namespace std;


template <class T>
class Node {
public:
	T data;
	Node<T>* next, * prev;
	
	Node(T el, Node<T>* n = 0, Node<T>* p = 0)
	{
		data = el; next = n; prev = p;
	}
};

template <class T>
class List {
public:
	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	~List();
	void pushToHead(T);
	void pushToTail(T);
	T popHead();
	T popTail();
	bool search(T);
	void print();
private:
	Node<T>* head, * tail;
};

#endif

template <class T>
List<T>::~List() {
	for (Node<T>* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

template <class T>
void List<T>::pushToHead(T el)
{
	head = new Node<T>(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}

template <class T>
void List<T>::pushToTail(T el)
{
	tail = new Node<T>(el, 0, tail);
	if (head == 0) {
		head = tail;
	}
	else {
		tail->prev->next = tail;
	}
}

template <class T>
T List<T>::popHead()
{
	T el = head->data;
	Node<T>* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}

template <class T>
T List<T>::popTail()
{
	T el = tail->data;
	Node<T>* tmp = tail;
	if (head == tail) {
		head = tail = 0;
	}
	else {
		tail = tail->prev;
		tail->next = 0;
	}
	delete tmp;
	return el;
}

template <class T>
bool List<T>::search(T el)
{
	bool check = false;
	Node<T>* tmp1 = head;
	Node<T>* tmp2 = tail;
	while (check == false) {
		if (tmp1->data == el || tmp2->data == el) { //search 2 way from head->tail and tail->head.
			check = true;
		}
		if (tmp1 == tmp2 || tmp1->next == tmp2 || tmp2->prev == tmp1) { //do not use to much processing.
			break;
		}
		tmp1 = tmp1->next;
		tmp2 = tmp2->prev;
	}
	return check;
}

template <class T>
void List<T>::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node<T>* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
